const currencyApi = "https://open.er-api.com/v6/latest/USD";
const usdCurrencyFormatOptions = {style: 'currency', currency: 'USD', maximumFractionDigits: 0, minimumFractionDigits: 0};
const currencyFormatOptions = {style: 'currency', currency: 'USD', maximumFractionDigits: 0, minimumFractionDigits: 0};
const dateFormat = { weekday: 'short', year: 'numeric', month: 'long', day: 'numeric', timeZone: 'UTC', timeZoneName: 'short', hour: '2-digit', minute: '2-digit' };
let currencyData = JSON.parse(localStorage.getItem('currencyData') || '{"result": "new", "time_next_update_unix": 0}');
let retrieving = false;
let markup = "";
let dialog;

function generateMarkup(props) {
    let url = props.url || "";
    let title = props.title || "";
    let description = props.description || "";
    let creator = props.creator || "";
    let amount = props.amount || ""
    let convertedAmount = props.convertedAmount || "";
    let goal = props.goal || "";
    let convertedGoal = props.convertedGoal || "";
    let deadline = props.deadline || "";
    let miniGraphic = props.miniGraphic || "";
    return `[url=${url}][size=18][u][b]${title}[/b][/u][/size][/url]
[q]${description}[/q]

Project by ${creator}

${amount} ${convertedAmount}pledge level to get the game

This project will only be funded if it reaches its ${goal} ${convertedGoal}goal by ${deadline}.

${miniGraphic}`;
}

function retrieveProjectData() {
    if (retrieving) {
        return;
    }
    retrieving = true;
    $("#error").hide();
    let url = document.URL;
    let urlParts = url.split('?');
    if (urlParts.length >= 2) {
      url = urlParts[0];
    }
    if (url.match("^https://www.kickstarter.com") != null) {
        // Kickstarter
            let projectData = JSON.parse(document.querySelector("#react-project-header").getAttribute("data-initial"));
            currencyFormatOptions.currency = projectData.project.currency;
            let currencyFormat = new Intl.NumberFormat('en-US', currencyFormatOptions);
            let usdCurrencyFormat = new Intl.NumberFormat('en-US', usdCurrencyFormatOptions);
            let goal = currencyFormat.format(parseFloat(projectData.project.goal.amount));
            let currency = projectData.project.currency;
            data = {
                url: url,
                title: projectData.project.name,
                description: projectData.project.description,
                creator: projectData.project.creator.name,
                currency: projectData.project.goal.currency,
                goal: goal,
                deadline: new Date(projectData.project.deadlineAt * 1000).toLocaleString("en-US", dateFormat),
                miniGraphic: `[url=https://www.kicktraq.com/projects/${projectData.project.slug}/][img]https://www.kicktraq.com/projects/${projectData.project.slug}/minichart.png[/img][/url]`
            }
            let pledges = parsePledges();
            let exchangeRate = currencyData.rates[projectData.project.currency];
            if (currency !== "USD") {
                data.convertedGoal = `(~${usdCurrencyFormat.format(parseFloat(projectData.project.goal.amount) / exchangeRate)}) `;
            }
            console.log("Got project data", projectData);
            if (pledges.length > 1) {

                let pledgeHtml = "<legend>Select main pledge: </legend>";
                for (const pledge of pledges) {
                    let amount = `${currencyFormat.format(pledge.amount)}`
                    let convertedAmount = currency !== "USD" ? `(~${usdCurrencyFormat.format(pledge.amount / exchangeRate)}) ` : "";
                    pledgeHtml += `<label for="pledge-${pledge.id}">${amount} ${pledge.title}</label><input type="radio" name="pledge" id="pledge-${pledge.id}" data-amount="${amount}" data-converted-amount="${convertedAmount}">`;
                }
                $("#bgg-pledges").html(pledgeHtml);
                $("#bgg-pledges input").checkboxradio({icon: false});
                $("#bgg-pledges").data('props', data);
                dialog.dialog("open");
            } else {
                data.amount = `${currencyFormat.format(pledges[0].amount)}`;
                if (currency !== "USD") {
                    data.convertedAmount = `(~${usdCurrencyFormat.format(pledges[0].amount / exchangeRate)}) `;
                }
                markup = generateMarkup(data);
                copyToClipboard(markup);
            }
            retrieving = false;
    } else {
        error("This generator currently supports only Kickstarter projects");
        retrieving = false;
    }
    previousUrl = url;
}

function parsePledges() {
    return Array.from(document.querySelectorAll("#react-rewards-sidebar li")).map(val => {
        let amount = parseInt(val.querySelector("div header div p").textContent.replace(/\D/g, ''), 10);
        let title = val.querySelector("header h3").textContent.trim();
        let id = val.querySelector("article").getAttribute("data-test-id");
        return {amount: amount, title: title, id: id};
    });
}

function selectPledge() {
    let pledge = $("#bgg-pledges input:checked").eq(0);
    let data = $("#bgg-pledges").data('props');
    console.log("Pledge", pledge, pledge.data('amount'), pledge.data('converted-amount'));
    data.amount = pledge.data('amount');
    data.convertedAmount = pledge.data('converted-amount');
    markup = generateMarkup(data);
    console.log("Generated markup", markup);
    copyToClipboard(markup);
    retrieving = false;
    dialog.dialog("close");
}

function error(err) {
    $("#error").text(err).show();
}

function cacheCurrencyData(currency) {
    localStorage.setItem('currencyData', JSON.stringify(currency));
    currencyData = currency;
}
function init() {
  var uiCss = document.createElement("link");
  uiCss.setAttribute("rel", "stylesheet");
  uiCss.setAttribute("href", "https://code.jquery.com/ui/1.13.0/themes/smoothness/jquery-ui.css");
  document.querySelector("head").append(uiCss);
  if (currencyData.result !== "success" || currencyData.time_next_update_unix < Math.floor(Date.now() / 1000)) {
      fetch(currencyApi).then(r => { return r.json();}).then(cacheCurrencyData)
  }
  var markupButtonContainer = document.createElement("div");
  markupButtonContainer.className = "flex mb3";
  var markupButton = document.createElement("div");
  markupButton.innerHTML = "Generate GeekList Markup";
  markupButton.id = "bgg-generate";
  markupButton.className = "bttn bttn-medium bttn-secondary theme--support w100p fill-bttn-icon hover-fill-bttn-icon keyboard-focusable";
  var dialogElement = document.createElement("div");
  dialogElement.id = "bgg-pledge-dialog";
  var pledges = document.createElement("fieldset");
  var container = document.querySelector("div#main_content div.grid-container div.py0");
  pledges.id = "bgg-pledges";
  dialogElement.append(pledges);
  document.querySelector("body").append(dialogElement);
  console.log("Created dialog div", document.querySelector("#bgg-pledge-dialog"));
  markupButtonContainer.append(markupButton);
  container.append(markupButtonContainer);
  console.log("Created markup button", document.querySelector("#bgg-generate"), markupButtonContainer.parentNode === container);
  $("#bgg-generate").click(retrieveProjectData);
  dialog = $("#bgg-pledge-dialog").dialog({
      dialogClass: "no-close",
      autoOpen: false,
      height: 400,
      width: 350,
      modal: true,
      buttons: {
          "Select main pledge": selectPledge,
      }
  });
  $("#bgg-pledges").controlgroup();
}

$(function(){
  setTimeout(init, 2000);
});


// Credited to https://stackoverflow.com/a/33928558
function copyToClipboard(text) {
  navigator.permissions.query({ name: 'clipboard-write' }).then(result => {
      if (result.state === 'granted') {
          var blob = new Blob([text], {type: 'text/plain'});
          var item = new ClipboardItem({'text/plain': blob});
          navigator.clipboard.write([item]).then(function() {
            console.log("Copied to clipboard successfully!");
          }, function(error) {
            console.error("unable to write to clipboard. Error:");
            console.log(error);
          });
      } else {
          console.log("clipboard-permission not granted: " + result);
      }
  });
}

console.log("Hello there. You are browsing Kickstarter project", document.querySelector("h2.project-name").textContent);
